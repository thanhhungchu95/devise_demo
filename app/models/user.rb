class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable,
  # :recoverable, :trackable
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  enum role: {user: 0, admin: 1, guest: 2}

  validates :email, presence: true
  validates :password, presence: true
end
